# Keycloak Config

This repository contains:

* configuration for Keycloak components across local, sandbox, dev,
   staging, and production environments;
* scripts for generating new config and for deploying changes to different
   environments.

## Configuration Overview

The `json/` directory contains contains `configuration_[ENVIRONMENT].json` files which define environment-specific values for Keycloak components where needed. For example, each client has different `root_url`s per environment, so the different values for each env are specified in the respective environment configuration file. The `json/` directory also contains subdirectories for each type of component configured in Keycloak, with JSON configuration for each defined component. For example, the `json/clients/` directory contains a config file for each of the clients defined in Keycloak.

Also note that some clients require additional configuration as explained in [this issue](https://gitlab.com/calincs/infrastructure/keycloak-config/-/issues/16).

## Adding New Configuration

There are two options for adding new configuration to this repository: export an
existing config, or run one of the provided config creation scripts.

### Export Existing Config from Keycloak

To export configuration from the Keycloak Admin Console, click on "Export" from
the sidebar and select what data you want to export. You may need to separate the
resulting JSON file into multiple files, one per component. Add the new config to
the appropriate subdirectories under `json/`.

### Create New Config using Scripts

To create a new config, run the TUI from the repository root: `python keycloak_config_TUI.py`. Currently, only new client creation is supported.

## Updating Configuration

To update existing configuration, edit the appropriate config file under the `json/` directory.

## Deploying Changes Locally

To run the deployment script, you will need to export the following environment variables:

* `KC_SERVER_AUTH_URL`: The `/auth/` URL of the Keycloak server
* `KC_ADMIN_USER`: The admin username
* `KC_ADMIN_PASSWORD`: The admin password
* `KC_REALM`: The realm in which the configuration should be deployed

Then run the script as follows:

```bash
python deploy_script.py
```

## Deploying Changes to LINCS Environments

Changes to the LINCS sandbox, staging, and production Keycloak instances are
deployed through Gitlab CI/CD. To submit an update for those environments,
create a PR with your changes against the `main` branch. Once the PR is
approved and merged, a new CI/CD pipeline will be created with three tasks
to deploy to each of sandbox, staging, and production. These tasks need to be
run manually by a Gitlab user with sufficient permissions.

If your update involves creating a new client, you will need to ask a Keycloak
admin to provide you with the client credentials once the update has been deployed.
