from keycloak import KeycloakAdmin
import os
import json

client_config_path='json/clients'
realm_role_config_path='json/roles/realm'
keycloak_admin = None

def get_configuration(auth_server):
  if auth_server == "https://keycloak.sandbox.lincsproject.ca/auth/":
    with open('json/configuration_sandbox.json') as config:
      return json.load(config)
  elif auth_server == "https://keycloak.stage.lincsproject.ca/auth/":
    with open('json/configuration_stage.json') as config:
      return json.load(config)
  elif auth_server == "https://keycloak.lincsproject.ca/auth/":
    with open('json/configuration_prod.json') as config:
      return json.load(config)
  elif auth_server == "https://keycloak.mirror.lincsproject.ca/auth/":
    with open('json/configuration_mirror.json') as config:
      return json.load(config)
  else:
    with open('json/configuration_local.json') as config:
      return json.load(config)

def init_admin(auth_server, realm, kc_username, kc_password):
  global keycloak_admin
  keycloak_admin = KeycloakAdmin(server_url = auth_server,
                                 username = kc_username,
                                 password = kc_password,
                                 verify=True)
  keycloak_admin.realm_name = realm

def import_client(filename, configuration):
  """Imports the client defined in the given file."""
  with open(filename, 'r') as client_description:
    file = filename.split("/")[len(filename.split("/")) - 1]
    payload = json.load(client_description)
    payload["rootUrl"] = configuration[file]["rootUrl"]
    payload["adminUrl"] = configuration[file]["adminUrl"]
    payload["baseUrl"] = configuration[file]["baseUrl"]
    payload["redirectUris"] = configuration[file]["redirectUris"]
    payload["webOrigins"] = configuration[file]["webOrigins"]

    clients = keycloak_admin.get_clients()
    for client in clients:
      if client["clientId"] == payload["clientId"]:
        return keycloak_admin.update_client(client["id"], payload)
      
    return keycloak_admin.create_client(payload, skip_exists = False)

def import_realm_role(filename, configuration):
  """Imports the role defined in the given file."""
  with open(filename, 'r') as role_description:
    payload = json.load(role_description)

    roles = keycloak_admin.get_realm_roles()
    for role in roles:
      if role["name"] == payload["name"]:
        return keycloak_admin.update_realm_role(role["name"], payload)
      
    return keycloak_admin.create_realm_role(payload, skip_exists = False)

def import_all(auth_server):
  """Imports all client and role definitions defined under the json/ directory."""
  configuration = get_configuration(auth_server)
  for config_file in os.listdir(client_config_path):
    import_client(client_config_path + '/' + config_file, configuration)

  for config_file in os.listdir(realm_role_config_path):
    import_realm_role(realm_role_config_path + '/' + config_file, configuration)
