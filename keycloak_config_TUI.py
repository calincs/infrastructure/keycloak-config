import urwid
import sys
import json

title = urwid.Text("\nCreate a New Client", 'center')
fill = urwid.Text("")
payload = {}
with open("json/configuration_local.json") as config:
    configuration_local=json.load(config)
with open("json/configuration_stage.json") as config:
    configuration_stage=json.load(config)
with open("json/configuration_prod.json") as config:
    configuration_prod=json.load(config)

def add_client_info():
    global client_id
    global name
    global desc
    global enabled_op1
    global access_op1
    global access_op2
    global access_op3
    global auth_op1

    payload["clientId"] = client_id.get_edit_text()
    payload["name"] = name.get_edit_text()
    payload["description"] = desc.get_edit_text()
    payload["enabled"] = enabled_op1.get_state()

    if access_op1.get_state() == True:
        payload["publicClient"] = True
        payload["bearerOnly"] = False
    elif access_op2.get_state() == True:
        payload["publicClient"] = False
        payload["bearerOnly"] = False

        payload["authorizationServicesEnabled"] = auth_op1.get_state()
        payload["serviceAccountsEnabled"] = auth_op1.get_state()
    elif access_op3.get_state() == True:
        payload["publicClient"] = False
        payload["bearerOnly"] = True

def add_static_client_info():
    payload["alwaysDisplayInConsole"] = False
    payload["consentRequired"] = False

    if payload["bearerOnly"] == False:
        payload["standardFlowEnabled"] = True
        payload["implicitFlowEnabled"] = False
        payload["directAccessGrantsEnabled"] = True

    payload["attributes"] = {}
    payload["attributes"]["backchannel.logout.session.required"] = False
    payload["attributes"]["backchannel.logout.revoke.offline.tokens"] = False
    payload["surrogateAuthRequired"] = False
    payload["clientAuthenticatorType"] = "client-secret"
    payload["notBefore"] = 0
    payload["frontchannelLogout"] = False
    payload["protocol"] = "openid-connect"
    payload["authenticationFlowBindingOverrides"] = {}
    payload["fullScopeAllowed"] = True
    payload["nodeReRegistrationTimeout"] = -1
    payload["defaultClientScopes"] = [
        "web-origins",
        "roles",
        "profile",
        "email"
    ]
    payload["optionalClientScopes"] = [
        "address",
        "phone",
        "offline_access",
        "microprofile-jwt"
    ]
    payload["access"] = {}
    payload["access"]["view"] = True
    payload["access"]["configure"] = True
    payload["access"]["manage"] = True

def add_config_info():
    global local_root
    global stage_root
    global prod_root

    configuration_local[payload["clientId"] + ".json"] = {}
    configuration_stage[payload["clientId"] + ".json"] = {}
    configuration_prod[payload["clientId"] + ".json"] = {}
    if local_root == None:
        configuration_local[payload["clientId"] + ".json"]["rootUrl"] = ""
        configuration_stage[payload["clientId"] + ".json"]["rootUrl"] = ""
        configuration_prod[payload["clientId"] + ".json"]["rootUrl"] = ""
        configuration_local[payload["clientId"] + ".json"]["adminUrl"] = ""
        configuration_stage[payload["clientId"] + ".json"]["adminUrl"] = ""
        configuration_prod[payload["clientId"] + ".json"]["adminUrl"] = ""
        configuration_local[payload["clientId"] + ".json"]["baseUrl"] = ""
        configuration_stage[payload["clientId"] + ".json"]["baseUrl"] = ""
        configuration_prod[payload["clientId"] + ".json"]["baseUrl"] = ""
        configuration_local[payload["clientId"] + ".json"]["redirectUris"] = ()
        configuration_stage[payload["clientId"] + ".json"]["redirectUris"] = ()
        configuration_prod[payload["clientId"] + ".json"]["redirectUris"] = ()
        configuration_local[payload["clientId"] + ".json"]["webOrigins"] = ()
        configuration_stage[payload["clientId"] + ".json"]["webOrigins"] = ()
        configuration_prod[payload["clientId"] + ".json"]["webOrigins"] = ()
    else:
        configuration_local[payload["clientId"] + ".json"]["rootUrl"] = local_root.get_edit_text()
        configuration_stage[payload["clientId"] + ".json"]["rootUrl"] = stage_root.get_edit_text()
        configuration_prod[payload["clientId"] + ".json"]["rootUrl"] = prod_root.get_edit_text()
        configuration_local[payload["clientId"] + ".json"]["adminUrl"] = configuration_local[payload["clientId"] + ".json"]["rootUrl"]
        configuration_stage[payload["clientId"] + ".json"]["adminUrl"] = configuration_stage[payload["clientId"] + ".json"]["rootUrl"]
        configuration_prod[payload["clientId"] + ".json"]["adminUrl"] = configuration_prod[payload["clientId"] + ".json"]["rootUrl"]
        configuration_local[payload["clientId"] + ".json"]["baseUrl"] = ""
        configuration_stage[payload["clientId"] + ".json"]["baseUrl"] = ""
        configuration_prod[payload["clientId"] + ".json"]["baseUrl"] = ""

        if configuration_local[payload["clientId"] + ".json"]["rootUrl"] == "":
            configuration_local[payload["clientId"] + ".json"]["redirectUris"] = ("", )
        else:
            configuration_local[payload["clientId"] + ".json"]["redirectUris"] = (configuration_local[payload["clientId"] + ".json"]["rootUrl"] + "/*", )

        if configuration_stage[payload["clientId"] + ".json"]["rootUrl"] == "":
            configuration_stage[payload["clientId"] + ".json"]["redirectUris"] = ("", )
        else:
            configuration_stage[payload["clientId"] + ".json"]["redirectUris"] = (configuration_stage[payload["clientId"] + ".json"]["rootUrl"] + "/*", )

        if configuration_prod[payload["clientId"] + ".json"]["rootUrl"] == "":
            configuration_prod[payload["clientId"] + ".json"]["redirectUris"] = ("", )
        else:
            configuration_prod[payload["clientId"] + ".json"]["redirectUris"] = (configuration_prod[payload["clientId"] + ".json"]["rootUrl"] + "/*", )

        configuration_local[payload["clientId"] + ".json"]["webOrigins"] = (configuration_local[payload["clientId"] + ".json"]["rootUrl"], )
        configuration_stage[payload["clientId"] + ".json"]["webOrigins"] = (configuration_stage[payload["clientId"] + ".json"]["rootUrl"], )
        configuration_prod[payload["clientId"] + ".json"]["webOrigins"] = (configuration_prod[payload["clientId"] + ".json"]["rootUrl"], )

def create_client():
    with open("json/clients/" + payload["clientId"] + ".json", "w") as output:
        json.dump(payload, output, indent=4)
    with open("json/configuration_local.json", "w") as output:
        json.dump(configuration_local, output, indent=4)
    with open("json/configuration_stage.json", "w") as output:
        json.dump(configuration_stage, output, indent=4)
    with open("json/configuration_prod.json", "w") as output:
        json.dump(configuration_prod, output, indent=4)

def gen_config(button):
    add_client_info()
    add_static_client_info()
    add_config_info()
    create_client()
    urwid.ExitMainLoop()
    sys.exit(0)

def next_btn(button):
    if access_op1.get_state() == True:
        urwid.MainLoop(next_screen_public(), palette=[('reversed', 'standout', '')]).run()
    elif access_op2.get_state() == True:
        urwid.MainLoop(next_screen_confidential(), palette=[('reversed', 'standout', '')]).run()
    elif access_op3.get_state() == True:
        urwid.MainLoop(next_screen_bearer(), palette=[('reversed', 'standout', '')]).run()

def cancel_btn(button):
    urwid.ExitMainLoop()
    sys.exit(0)

def next_screen_public():
    global local_root
    global stage_root
    global prod_root
    local_root = urwid.Edit("Local Root Url: ", "", False, 'left', 'ellipsis')
    local_root_padd = urwid.Padding(local_root, 'left', left=3)
    stage_root = urwid.Edit("Stage Root Url: ", "", False, 'left', 'ellipsis')
    stage_root_padd = urwid.Padding(stage_root, 'left', left=3)
    prod_root = urwid.Edit("Prod Root Url: ", "", False, 'left', 'ellipsis')
    prod_root_padd = urwid.Padding(prod_root, 'left', left=3)

    exit_tui = urwid.Button("  Cancel  ", cancel_btn)
    next_page = urwid.Button("  Finish  ", gen_config)
    options = urwid.Columns([(25, fill), (14, exit_tui), (2, fill), (14, next_page)])

    content = urwid.Pile([local_root_padd, fill, stage_root_padd, fill, prod_root_padd, fill, options])

    main = urwid.Frame(header=title, body=urwid.Filler(content))
    top = urwid.Overlay(main, urwid.SolidFill(u'\N{MEDIUM SHADE}'),
        align='center', width=80,
        valign='middle', height=20,
        min_width=100, min_height=20)

    return top

def next_screen_confidential():
    auth_btn = []

    global local_root
    global stage_root
    global prod_root
    global auth_op1
    local_root = urwid.Edit("Local Root Url: ", "", False, 'left', 'ellipsis')
    local_root_padd = urwid.Padding(local_root, 'left', left=3)
    stage_root = urwid.Edit("Stage Root Url: ", "", False, 'left', 'ellipsis')
    stage_root_padd = urwid.Padding(stage_root, 'left', left=3)
    prod_root = urwid.Edit("Prod Root Url: ", "", False, 'left', 'ellipsis')
    prod_root_padd = urwid.Padding(prod_root, 'left', left=3)
    auth_txt = urwid.Text("Authorization")
    auth_op1 = urwid.RadioButton(auth_btn, "Yes")
    auth_op2 = urwid.RadioButton(auth_btn, "No")
    auth = urwid.Padding(urwid.Pile([auth_txt, auth_op1, auth_op2]), 'left', left=3)

    exit_tui = urwid.Button("  Cancel  ", cancel_btn)
    next_page = urwid.Button("  Finish  ", gen_config)
    options = urwid.Columns([(25, fill), (14, exit_tui), (2, fill), (14, next_page)])

    content = urwid.Pile([local_root_padd, fill, stage_root_padd, fill, prod_root_padd, fill, auth, fill, options])

    main = urwid.Frame(header=title, body=urwid.Filler(content))
    top = urwid.Overlay(main, urwid.SolidFill(u'\N{MEDIUM SHADE}'),
        align='center', width=80,
        valign='middle', height=20,
        min_width=100, min_height=20)

    return top

def next_screen_bearer(): 
    finished_txt = urwid.Text("No Further Input Required", 'center')

    exit_tui = urwid.Button("  Cancel  ", cancel_btn)
    next_page = urwid.Button("  Finish  ", gen_config)
    options = urwid.Columns([(25, fill), (14, exit_tui), (2, fill), (14, next_page)])

    content = urwid.Pile([finished_txt, fill, options])

    main = urwid.Frame(header=title, body=urwid.Filler(content))
    top = urwid.Overlay(main, urwid.SolidFill(u'\N{MEDIUM SHADE}'),
        align='center', width=80,
        valign='middle', height=20,
        min_width=100, min_height=20)

    return top

def init_screen():
    global client_id
    global name
    global desc
    global enabled_op1
    global enabled_op2
    global access_op1
    global access_op2
    global access_op3

    enabled_btn = []
    access_btn = []

    client_id = urwid.Edit("Client Id: ", "", False, 'left', 'ellipsis')
    client_id_padd = urwid.Padding(client_id, 'left', left=3)
    name = urwid.Edit("Name: ", "", False, 'left', 'ellipsis')
    name_padd = urwid.Padding(name, 'left', left=3)
    desc = urwid.Edit("Description: ", "", False, 'left', 'ellipsis')
    desc_padd = urwid.Padding(desc, 'left', left=3)
    enabled_txt = urwid.Text("Enabled")
    enabled_op1 = urwid.RadioButton(enabled_btn, "Yes")
    enabled_op2 = urwid.RadioButton(enabled_btn, "No")
    enabled = urwid.Padding(urwid.Pile([enabled_txt, enabled_op1, enabled_op2]), 'left', left=3)
    access_txt = urwid.Text("Access Type")
    access_op1 = urwid.RadioButton(access_btn, "public")
    access_op2 = urwid.RadioButton(access_btn, "confidential")
    access_op3 = urwid.RadioButton(access_btn, "bearer")
    access = urwid.Padding(urwid.Pile([access_txt, access_op1, access_op2, access_op3]), 'left', left=3)

    exit_tui = urwid.Button("  Cancel  ", cancel_btn)
    next_page = urwid.Button("   Next   ", next_btn)
    options = urwid.Columns([(25, fill), (14, exit_tui), (2, fill), (14, next_page)])

    content = urwid.Pile([client_id_padd, fill, name_padd, fill, desc_padd, fill, enabled, fill, access, fill, options])

    main = urwid.Frame(header=title, body=urwid.Filler(content))
    top = urwid.Overlay(main, urwid.SolidFill(u'\N{MEDIUM SHADE}'),
        align='center', width=80,
        valign='middle', height=20,
        min_width=100, min_height=20)

    return top

urwid.MainLoop(init_screen(), palette=[('reversed', 'standout', '')]).run()