import os, sys
from keycloak_utils import init_admin, import_all

auth_server = os.getenv('KC_SERVER_AUTH_URL')
username = os.getenv('KC_ADMIN_USER')
password = os.getenv('KC_ADMIN_PASSWORD')
realm = os.getenv('KC_REALM')

# Check that env vars are set
if any(var is None for var in [auth_server, username, password, realm]):
  print("Required environment variables have not been set. Terminating script.")
  sys.exit(1)

# Deploy changes to env
print("Running script against %s" % auth_server)
init_admin(auth_server, realm, username, password)
import_all(auth_server)

print("Done.")
