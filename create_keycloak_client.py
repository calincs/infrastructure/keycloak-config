import os
import json

payload = {}
with open("json/configuration_local.json") as config:
    configuration_local=json.load(config)
with open("json/configuration_stage.json") as config:
    configuration_stage=json.load(config)
with open("json/configuration_prod.json") as config:
    configuration_prod=json.load(config)

def get_boolean_input():
    input_bool = str(input())
    if input_bool == "T" or input_bool == "t":
        return True
    else:
        return False

def public_user_input():
    payload["publicClient"] = True
    payload["bearerOnly"] = False

def bearer_only_user_input():
    payload["publicClient"] = False
    payload["bearerOnly"] = True
    
def confidential_user_input():
    payload["publicClient"] = False
    payload["bearerOnly"] = False

    payload["serviceAccountsEnabled"] = False
    print("Authorization Enabled (t/f):")
    payload["authorizationServicesEnabled"] = get_boolean_input()
    if payload["authorizationServicesEnabled"]:
        payload["serviceAccountsEnabled"] = True

def get_client_info():
    print("Keycloak Client Configuration: Generate Client")
    print("Client Id: ")
    payload["clientId"] = str(input())
    print("Name: ")
    payload["name"] = str(input())
    print("Description: ")
    payload["description"] = str(input())
    print("Enabled (t/f): ")
    payload["enabled"] = get_boolean_input()
    print("Access Type [(p)ublic/(c)onfidential/(b)earer-token]: ")
    access = str(input())

    if access[0] == "p" or access[0] == "P":
        public_user_input()
    elif access[0] == "b" or access[0] == "B":
        bearer_only_user_input()
    elif access[0] == "c" or access[0] == "C":
        confidential_user_input()
    else:
        print("You must enter a correct access type: (p)ublic/(c)onfidential/(b)earer-token")
        exit()

def add_static_client_info():
    payload["alwaysDisplayInConsole"] = False
    payload["consentRequired"] = False

    if payload["bearerOnly"] == False:
        payload["standardFlowEnabled"] = True
        payload["implicitFlowEnabled"] = False
        payload["directAccessGrantsEnabled"] = True

    payload["attributes"] = {}
    payload["attributes"]["backchannel.logout.session.required"] = False
    payload["attributes"]["backchannel.logout.revoke.offline.tokens"] = False
    payload["surrogateAuthRequired"] = False
    payload["clientAuthenticatorType"] = "client-secret"
    payload["notBefore"] = 0
    payload["frontchannelLogout"] = False
    payload["protocol"] = "openid-connect"
    payload["authenticationFlowBindingOverrides"] = {}
    payload["fullScopeAllowed"] = True
    payload["nodeReRegistrationTimeout"] = -1
    payload["defaultClientScopes"] = [
        "web-origins",
        "roles",
        "profile",
        "email"
    ]
    payload["optionalClientScopes"] = [
        "address",
        "phone",
        "offline_access",
        "microprofile-jwt"
    ]
    payload["access"] = {}
    payload["access"]["view"] = True
    payload["access"]["configure"] = True
    payload["access"]["manage"] = True

def get_config_info():
    configuration_local[payload["clientId"] + ".json"] = {}
    configuration_stage[payload["clientId"] + ".json"] = {}
    configuration_prod[payload["clientId"] + ".json"] = {}
    if payload["bearerOnly"] == True:
        configuration_local[payload["clientId"] + ".json"]["rootUrl"] = ""
        configuration_stage[payload["clientId"] + ".json"]["rootUrl"] = ""
        configuration_prod[payload["clientId"] + ".json"]["rootUrl"] = ""
        configuration_local[payload["clientId"] + ".json"]["adminUrl"] = ""
        configuration_stage[payload["clientId"] + ".json"]["adminUrl"] = ""
        configuration_prod[payload["clientId"] + ".json"]["adminUrl"] = ""
        configuration_local[payload["clientId"] + ".json"]["baseUrl"] = ""
        configuration_stage[payload["clientId"] + ".json"]["baseUrl"] = ""
        configuration_prod[payload["clientId"] + ".json"]["baseUrl"] = ""
        configuration_local[payload["clientId"] + ".json"]["redirectUris"] = ()
        configuration_stage[payload["clientId"] + ".json"]["redirectUris"] = ()
        configuration_prod[payload["clientId"] + ".json"]["redirectUris"] = ()
        configuration_local[payload["clientId"] + ".json"]["webOrigins"] = ()
        configuration_stage[payload["clientId"] + ".json"]["webOrigins"] = ()
        configuration_prod[payload["clientId"] + ".json"]["webOrigins"] = ()
    else:
        print("")
        print("Keycloak Client Configuration: Generate Environment Configuration")
        print("Local Root Url: ")
        configuration_local[payload["clientId"] + ".json"]["rootUrl"] = str(input())
        print("Stage Root Url: ")
        configuration_stage[payload["clientId"] + ".json"]["rootUrl"] = str(input())
        print("Production Root Url: ")
        configuration_prod[payload["clientId"] + ".json"]["rootUrl"] = str(input())
        configuration_local[payload["clientId"] + ".json"]["adminUrl"] = configuration_local[payload["clientId"] + ".json"]["rootUrl"]
        configuration_stage[payload["clientId"] + ".json"]["adminUrl"] = configuration_stage[payload["clientId"] + ".json"]["rootUrl"]
        configuration_prod[payload["clientId"] + ".json"]["adminUrl"] = configuration_prod[payload["clientId"] + ".json"]["rootUrl"]
        configuration_local[payload["clientId"] + ".json"]["baseUrl"] = ""
        configuration_stage[payload["clientId"] + ".json"]["baseUrl"] = ""
        configuration_prod[payload["clientId"] + ".json"]["baseUrl"] = ""

        if configuration_local[payload["clientId"] + ".json"]["rootUrl"] == "":
            configuration_local[payload["clientId"] + ".json"]["redirectUris"] = ("", )
        else:
            configuration_local[payload["clientId"] + ".json"]["redirectUris"] = (configuration_local[payload["clientId"] + ".json"]["rootUrl"] + "/*", )

        if configuration_stage[payload["clientId"] + ".json"]["rootUrl"] == "":
            configuration_stage[payload["clientId"] + ".json"]["redirectUris"] = ("", )
        else:
            configuration_stage[payload["clientId"] + ".json"]["redirectUris"] = (configuration_stage[payload["clientId"] + ".json"]["rootUrl"] + "/*", )

        if configuration_prod[payload["clientId"] + ".json"]["rootUrl"] == "":
            configuration_prod[payload["clientId"] + ".json"]["redirectUris"] = ("", )
        else:
            configuration_prod[payload["clientId"] + ".json"]["redirectUris"] = (configuration_prod[payload["clientId"] + ".json"]["rootUrl"] + "/*", )

        configuration_local[payload["clientId"] + ".json"]["webOrigins"] = (configuration_local[payload["clientId"] + ".json"]["rootUrl"], )
        configuration_stage[payload["clientId"] + ".json"]["webOrigins"] = (configuration_stage[payload["clientId"] + ".json"]["rootUrl"], )
        configuration_prod[payload["clientId"] + ".json"]["webOrigins"] = (configuration_prod[payload["clientId"] + ".json"]["rootUrl"], )

def get_user_input():
    get_client_info()
    add_static_client_info()
    get_config_info()
    

def create_client():
    with open("json/clients/" + payload["clientId"] + ".json", "w") as output:
        json.dump(payload, output, indent=4)
    with open("json/configuration_local.json", "w") as output:
        json.dump(configuration_local, output, indent=4)
    with open("json/configuration_stage.json", "w") as output:
        json.dump(configuration_stage, output, indent=4)
    with open("json/configuration_prod.json", "w") as output:
        json.dump(configuration_prod, output, indent=4)

get_user_input()
create_client()
